import React, { useState } from 'react'
import {View, Text, TouchableOpacity, StyleSheet, TextInput, Modal} from 'react-native'

import Header from './components/Header'

import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default function Detail(props){
    const param = props.route.params
    return(
        <View>
            <LinearGradient colors={["#2580B2",'#15A0BE', '#bbb']} style={{ width: '100%', height: '100%'}}>
            <Header/>
            <View style={styles.detailContainer}>
                <Text style={styles.detailTitle}>Nama Barang:</Text>
                <Text style={styles.detailDesc}>{param.itemName}</Text>
                <Text style={styles.detailTitle}>Jumlah Barang:</Text>
                <Text style={styles.detailDesc}>{param.itemAmount}</Text>
                <Text style={styles.detailTitle}>Tanggal Masuk Barang:</Text>
                <Text style={styles.detailDesc}>{param.inputDate}</Text>
                <Text style={styles.detailTitle}>Tanggal Kadaluarsa Barang:</Text>
                <Text style={styles.detailDesc}>{param.expiredDate}</Text>

                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={styles.detailButton} onPress={()=> props.navigation.navigate('List')}>
                        <Text style={styles.buttonText}>Kembali</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.detailButton}>
                        <Text style={styles.buttonText}>Hapus</Text>
                    </TouchableOpacity>
                </View> 

            </View>
            </LinearGradient>
        </View>
    )
}

const styles = StyleSheet.create({
    detailContainer:{
        backgroundColor: '#fff',
        paddingHorizontal: wp('5%'),
        paddingVertical: hp('2%'),
        marginTop: hp('1.5%')
    },
    detailTitle: {
        fontSize: hp('2.5%'),
        fontWeight: 'bold',
        paddingBottom: hp('1%')
    },
    detailDesc: {
        fontSize: hp('2.5%'),
        paddingBottom: hp('1%')
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: hp('2%')
    },
    detailButton:{
        backgroundColor: '#15A0BE',
        width: '40%',
        paddingVertical: hp('1%'),
        borderRadius: 10
    },
    buttonText:{
        fontSize: hp('2.5%'),
        textAlign: 'center',
        color: '#fff'
    }
})