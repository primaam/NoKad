import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp} from 'react-native-responsive-screen';
// import {LinearGradient} from 'react-native-linear-gradient';


function SplashScreen(props){
    return (
        <View style={styles.background}>
            <Text style={styles.text}>NoKad</Text>
        </View>
    )
}

export default SplashScreen

const styles = StyleSheet.create({
    background:{
        height: '100%',
        backgroundColor: '#2580B2'
    },
    text: {
        justifyContent: 'center',
        alignContent: 'center',
        fontSize: 70,
        fontWeight: 'bold',
        color: '#fff'
    }
})