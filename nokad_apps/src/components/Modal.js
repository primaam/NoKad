import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp} from 'react-native-responsive-screen';

import LinearGradient from 'react-native-linear-gradient';

export default function ModalLayer(props){
    return(
    <View style={styles.modal}>
        <View style={styles.modalCore}> 
            
        </View>
    </View>
    )
}

const styles = StyleSheet.create({
    modal:{
        height: hp('100%'),
        width: wp('100%'),
        alignItems: 'center',
        backgroundColor: 'rgba(100,100,100, 0.5)',
        justifyContent: 'center',
        alignContent: 'center',
    },
    modalCore:{
        backgroundColor: '#fff', 
        marginHorizontal: wp('5%'),
        paddingHorizontal: wp('5%'), 
        paddingTop: hp('2%'), 
        paddingBottom: hp('5%'),
        borderRadius: 10,
    },
})