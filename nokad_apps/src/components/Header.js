import React from 'react'
import {Text, StyleSheet} from 'react-native'
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default function Header(){
    return(
        <Text style={styles.titleText}>Nokad</Text>
    )
}

const styles = StyleSheet.create({
    titleText:{
        fontSize: hp('6%'),
        fontWeight: 'bold',
        paddingTop: hp('3%'),
        textAlign: 'center',
        color: '#fff'
    }
})