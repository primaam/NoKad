import React, { useState } from 'react'
import {View, Text, TouchableOpacity, StyleSheet, TextInput, Modal, ScrollView, FlatList} from 'react-native'

import Header from './components/Header'

import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default function List(props){
    const [searchName, setSearchName]=useState('')
    const [filterVisible,setFilterVisible] = useState(false)
    const [detailVisible, setDetailVisible]= useState(false)
    const [check,setCheck] = useState(false)
    const [todos,setTodos]=useState([
        {itemName: 'Pocari Sweat kaleng', itemAmount: 5, inputDate: '20-02-2021', expiredDate: '30-09-2021', key: 1},
        {itemName: 'Pocari Sweat botol', itemAmount: 10, inputDate: '19-02-2021', expiredDate: '30-10-2021', key: 2},
    ])

    const pressHandler = (key) => {
        setDetailVisible(true),
        // setTodos(()=> {
        //     return console.log('nomor 1')
        // }),
        console.log('yooii')
    }
    return(
        <View>
            <LinearGradient colors={["#2580B2",'#15A0BE', '#bbb']} style={{ width: '100%', height: '100%'}}>
            <Header/>

            <View style={styles.searchContainer}>
                <TextInput 
                placeholder='Masukkan Nama Barang'/>
                <TouchableOpacity style={styles.filterContainer} onPress={() => setFilterVisible(true)}>    
                    <Icon 
                    style={styles.filter}
                    name='filter'
                    />
                </TouchableOpacity>
            </View>

            {/* <FlatList 
            data={todos}
            renderItem={({item})=> (
                <View style={styles.listContainer}>
                    <TouchableOpacity style={styles.listDetail} 
                    onPress={(item)=> setDetailVisible(true, item)}
                    // onPress={({item, index})=> navigation.navigate('Detail', index)}
                    >
                        <Text style={styles.nameList}>{item.itemName}</Text>
                        <Text style={styles.expiredList}>{item.expiredDate}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.checklist} onPress={()=> setCheck(!check)}>
                        <Icon style={styles.checklistIcon} name={check != item.key? 'checkmark-circle-outline' : 'close-circle-outline'}/>
                    </TouchableOpacity>
                </View>
            )}
            /> */}

            <ScrollView style={{height: '100%'}}>
            {todos.map((item , index)=> (
                <View style={styles.listContainer}>
                    <TouchableOpacity 
                    style={styles.listDetail} 
                    // onPress={()=> setDetailVisible(true, item)}
                    onPress={()=> props.navigation.navigate('Detail', item)} 
                    key={index}>
                        <Text style={styles.nameList}>{item.itemName}</Text>
                        <Text style={styles.expiredList}>{item.expiredDate}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.checklist}>
                        <Icon style={styles.checklistIcon} name='checkmark-circle-outline'/>
                    </TouchableOpacity>
                </View>
            ))}
            </ScrollView>
            </LinearGradient>
            <Modal 
            animationType='fade'
            transparent={true}
            modalBackground='blur'
            visible={filterVisible}
            >
                <View style={styles.modal}>
                    <View style={styles.modalCore}> 
                        <View style={styles.filterList}>
                            <Text style={styles.filterText}>Nama Barang A-Z</Text>
                            <TouchableOpacity onPress={()=> setFilterVisible(false)}>
                                <Icon name='radio-button-on' style={styles.filterIcon}/>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.filterList}>
                            <Text style={styles.filterText}>Nama Barang Z-A</Text>
                            <TouchableOpacity onPress={()=> setFilterVisible(false)}>
                                <Icon name='radio-button-off' style={styles.filterIcon}/>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.filterList}>
                            <Text style={styles.filterText}>Tanggal Kadaluarsa A-Z</Text>
                            <TouchableOpacity onPress={()=> setFilterVisible(false)}>
                                <Icon name='radio-button-off' style={styles.filterIcon}/>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.filterList}>
                            <Text style={styles.filterText}>Tanggal Kadaluarsa Z-A</Text>
                            <TouchableOpacity onPress={()=> setFilterVisible(false)}>
                                <Icon name='radio-button-off' style={styles.filterIcon}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
            {/* <Modal
            animationType='fade'
            transparent={true}
            modalBackground='blur'
            visible={detailVisible}
            >
                 <View style={styles.modal}>
                    <View style={styles.modalCore}>

                        
                            
                        <View 
                        style={styles.detailContainer}
                        >                            
                            <Text style={styles.detailTitle}>Nama Barang:</Text>
                            <Text style={styles.detailDesc}>{todos.itemName}</Text>
                            <Text style={styles.detailTitle}>Jumlah Barang:</Text>
                            <Text style={styles.detailDesc}>{todos.itemAmount}</Text>
                            <Text style={styles.detailTitle}>Tanggal Masuk Barang:</Text>
                            <Text style={styles.detailDesc}>{todos.inputDate}</Text>
                            <Text style={styles.detailTitle}>Tanggal Kadaluarsa Barang:</Text>
                            <Text style={styles.detailDesc}>{todos.expiredDate}</Text>        
                        </View>
                         
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity style={styles.detailButton} onPress={()=> setDetailVisible(false)}>
                                <Text style={styles.buttonText}>Kembali</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.detailButton}>
                                <Text style={styles.buttonText}>Hapus</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal> */}
        </View>
    )
}

const styles = StyleSheet.create({
    searchContainer:{
        width: '90%',
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        alignSelf: 'center',
        borderRadius: 10,
        paddingLeft: wp('5%'),
        marginVertical: hp('2%'),
        paddingVertical: hp('1%')
    },
    filterContainer:{
        width: '15%'
    },
    filter:{
        fontSize: hp('3%'),
        borderLeftWidth: 1,
        textAlign: 'center'
    },
    listContainer: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingVertical: hp('2.5%'),
        borderTopWidth: 1,
        borderBottomWidth: 1,
    },
    listDetail: {
        width: '85%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: wp('5%'),
    },
    nameList:{
        fontSize: hp('2.3%'),
    },
    expiredList: {
        fontSize: hp('2.3%')
    },
    checklist: {
        width: '15%',
        borderLeftWidth: 1
    },
    checklistIcon:{
        fontSize: hp('3'),
        textAlign: 'center',
    },
    modal:{
        height: hp('100%'),
        width: wp('100%'),
        alignItems: 'center',
        backgroundColor: 'rgba(100,100,100, 0.5)',
        justifyContent: 'center',
        alignContent: 'center',
    },
    modalCore:{
        backgroundColor: '#fff', 
        marginHorizontal: wp('5%'),
        paddingHorizontal: wp('5%'), 
        paddingVertical: hp('5%'),
        borderRadius: 10,
    },
    filterList:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: hp('2%'),
        width: '100%'
    },
    filterText:{
        fontSize: hp('3%'),
        width: '90%'
    },
    filterIcon: {
        fontSize: hp('3%'),
        width: '100%'
    },
    detailContainer:{
        width: '90%',
        borderBottomWidth: 1,
        paddingBottom: hp('2%')
    },
    detailTitle: {
        fontSize: hp('2.5%'),
        fontWeight: 'bold',
        paddingBottom: hp('1%')
    },
    detailDesc: {
        fontSize: hp('2.5%'),
        paddingBottom: hp('1%')
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: hp('2%')
    },
    detailButton:{
        backgroundColor: '#15A0BE',
        width: '40%',
        paddingVertical: hp('1%'),
        borderRadius: 10
    },
    buttonText:{
        fontSize: hp('2.5%'),
        textAlign: 'center',
        color: '#fff'
    }
})