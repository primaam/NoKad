import React, { useState } from 'react'
import {View, Text, TouchableOpacity, StyleSheet, TextInput, Modal} from 'react-native'

import Header from './components/Header'

import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import DateTimePicker from '@react-native-community/datetimepicker';


function AddItems(){
    const [inputItemsModal, setInputItemsModal]= useState(false)
    const [inputExpiredModal, setInputExpiredModal]= useState(false)
    const [itemName, setItemName] = useState('')
    const [itemAmount, setItemAmount] = useState('')
    const [inputDate, setInputDate]= useState()
    const [expiredDate, setExpiredDate]=useState()
    const [isShowInputDate, setIsShowInputDate] = useState(false);
    const [isShowExpiredDate, setIsShowExpiredDate] = useState(false);
    const [message, setMessage] = useState('');

    const convertDateToString = (selectedDate) => {
        const currDate = ('0' + selectedDate.getDate()).slice(-2);
        const currMonth = selectedDate.getMonth() + 1;
        const currYear = selectedDate.getFullYear();
        return currDate + '-' + currMonth + '-' + currYear
    }

    const onChangeInputDate = (event, selectedDate) => {
        setIsShowInputDate(false)
        const currentDate = selectedDate || inputDate;
        setInputDate(currentDate);
    };

    const onChangeExpiredDate = (event, selectedDate) => {
        setIsShowExpiredDate(false)
        const currentDate = selectedDate || expiredDate;
        setExpiredDate(currentDate);
    };

    const submit = () => {
        if(!itemName){
            alert ('Data Belum Lengkap')
        } else if(!itemAmount){
            alert('Data Belum Lengkap')
        } else if(!inputDate){
            alert('Data Belum Lengkap')
        }else if (!expiredDate){
            alert('Data Belum Lengkap')
        } else{
            setMessage(null)
        }
    }

    return(
        <View styles={styles.container}>
            <LinearGradient colors={["#2580B2",'#15A0BE', '#bbb']} style={{ width: '100%', height: '100%'}}>
            <Header/>

            <View style={styles.formContainer}>
                <TextInput 
                placeholder='Nama Barang' 
                style={styles.inputBar} 
                onChangeText={(text)=> setItemName(text)}
                value={itemName}/>
                <TextInput 
                placeholder='Jumlah Barang (Unit)' 
                style={styles.inputBar}
                onChangeText={(text)=> setItemAmount(text)}
                value={itemAmount}/>
                
                <View style={styles.scheduleContainer}>
                    <Text style={styles.inputSchedule}>{inputDate? convertDateToString(inputDate) : 'Tanggal Masuk Barang'}</Text>
                    <TouchableOpacity onPress={() => setIsShowInputDate(true)}>
                        <Icon name='calendar' style={styles.formIcon}/>
                    </TouchableOpacity>
                    {isShowInputDate && (
                    <DateTimePicker
                    testID="dateTimePicker"
                    value={inputDate? inputDate : new Date()}
                    mode={'date'}
                    is24Hour={true}
                    display="default"
                    onChange={onChangeInputDate}
                    />
                    )}
                </View>
                
                <View style={styles.scheduleContainer}>
                    <Text style={styles.inputSchedule}>{expiredDate? convertDateToString(expiredDate) : 'Tanggal Kadaluarsa'}</Text>
                    <TouchableOpacity onPress={() => setIsShowExpiredDate(true)}>
                        <Icon name='calendar' style={styles.formIcon}/>
                    </TouchableOpacity>
                    {isShowExpiredDate && (
                    <DateTimePicker
                    testID="dateTimePicker"
                    value={expiredDate? expiredDate : new Date()}
                    mode={'date'}
                    is24Hour={true}
                    display="default"
                    onChange={onChangeExpiredDate}
                    />
                    )}
                </View>
            </View>

            <TouchableOpacity 
            style={styles.buttonContainer}
            // onPress={() => submit()}
            >
                <Text style={styles.buttonTitle}>Tambahkan</Text>
            </TouchableOpacity>

            </LinearGradient>
        </View>
    )
}

export default AddItems

const styles= StyleSheet.create({
    container:{
        backgroundColor: '#15A0BE',
        height: '100%',
    },
    form:{

    },
    formContainer: {
        marginTop: hp('15%'),
        marginBottom: hp('5%')
    },
    inputBar:{
        width: '90%',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignSelf: 'center',
        paddingVertical: hp('2%'),
        paddingHorizontal: wp('5%'),
        borderRadius: 10,
        borderWidth: 1,
        marginBottom: hp('2%')
    },
    formIcon:{
        fontSize: hp('3.5%'),
        paddingRight: wp('4%'),
        paddingLeft: wp('2.5%'),
        borderLeftWidth: 1
    },
    inputSchedule:{
        paddingHorizontal: wp('5%'),
        paddingVertical: hp('2%')
    },
    scheduleContainer:{
        flexDirection: 'row',
        backgroundColor: '#fff',
        width: '90%',
        justifyContent: 'space-between',
        alignSelf: 'center',
        alignItems: 'center',
        borderRadius: 10,
        borderWidth: 1,
        marginBottom: hp('2%')
    },
    buttonContainer:{
        width: '90%',
        backgroundColor: '#2580B2',
        alignSelf: 'center',
        borderRadius: 10
    },
    buttonTitle:{
        textAlign: 'center',
        fontSize: hp('3%'),
        fontWeight: 'bold',
        paddingVertical: hp('1.5%'),
        color: '#fff'
    }
})