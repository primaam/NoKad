import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../Home'
import AddItems from '../AddItmes'
import List from '../List'
import Detail from '../Detail';


const Stack = createStackNavigator();


export default function AppStack(){
    return(
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen 
                options={{headerShown: false}}
                name="Home" 
                component={Home} />
                <Stack.Screen 
                options={{headerShown: false}}
                name="Add" 
                component={AddItems} />
                <Stack.Screen 
                options={{headerShown: false}}
                name="List" 
                component={List} />
                <Stack.Screen 
                options={{headerShown: false}}
                name="Detail" 
                component={Detail} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}