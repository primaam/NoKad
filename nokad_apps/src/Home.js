import React from 'react'
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native'
import Header from './components/Header'
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp} from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';

function Home(props){
    return(
        <View style={styles.container}>
            <LinearGradient colors={["#2580B2",'#15A0BE', '#bbb']} style={{ width: '100%', height: '100%'}}>
            <Header/>
            <View style={styles.buttonContainer}>
                <TouchableOpacity style={styles.button} onPress={() => props.navigation.navigate('Add')}>
                    <Text style={styles.caption}>Tambahkan Barang</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={() => props.navigation.navigate('List')}>
                    <Text style={styles.caption}>Daftar Barang</Text>
                </TouchableOpacity>
            </View>
            </LinearGradient>
        </View>
    )
}

export default Home

const styles = StyleSheet.create({
    container:{
        height: '100%',
    },
    buttonContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    button:{
        alignSelf: 'center',
        backgroundColor: '#C4C4C4',
        borderRadius: 20,
        width: wp('80%'),
        paddingVertical: hp('2%'),
        marginVertical: hp('3%')
    },
    caption:{
        textAlign: 'center',
        fontSize: hp('3%'),
        color: '#fff',
        fontWeight: 'bold'
    }
})


